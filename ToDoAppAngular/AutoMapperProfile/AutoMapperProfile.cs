﻿using System;
using AutoMapper;
using ToDoAppAngular.Models;
using ToDoAppAngular.ViewModel;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<ToDo, TodoAddViewModel>().ReverseMap();
        CreateMap<ToDo, TodoUpdateViewModel>().ReverseMap();
    }
}
