﻿using System;
using ToDoAppAngular.Models;

namespace ToDoAppAngular.ViewModel
{
    public class TodoAddViewModel
    {
        public string WorkTodo { get; set; }
        public string DescriptionTodo { get; set; }
        public string TitleTodo { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime?  StartTime{ get; set; }
        public DateTime?  EndTime{ get; set; }
        public User User { get; set; }
    }

    public class TodoUpdateViewModel
    {
        public string WorkTodo { get; set; }
        public bool IsCompleted { get; set; }
        public string TitleTodo { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime?  StartTime{ get; set; }
        public DateTime?  EndTime{ get; set; }
        public User User { get; set; }
    }
}