﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using AutoMapper;
using ToDoAppAngular.Service;
using ToDoAppAngular.Models;
using ToDoAppAngular.ViewModel;
using Microsoft.Extensions.Logging;

namespace ToDoAppAngular.Controllers
{
    [Route("api/todo")]
    public class ToDoController : Controller
    {
        private readonly IToDoService _todoService;
        private readonly IMapper _mapper;


        public ToDoController(IToDoService todoService, IMapper mapper)
        {
            this._todoService = todoService;
            this._mapper = mapper;
        }
        
        [Route("getTodoAll")]
        [HttpGet]
        public async Task<IEnumerable<ToDo>> GetTodos()
        {
            // Note: If you don't need to return to the calling thread, use .ConfigureAwait(false)
            return await _todoService.GetAll().ConfigureAwait(false);
        }
        [Route("addtodo")]
        [HttpPost]
        public async Task<IActionResult> AddTodo(TodoAddViewModel todo) //[FromBody] 
        {
            if (todo != null)
            {
                // NOTE: Used AutoMapper for Object-to-Object mapping instead of old school viewModel 
                var mappedViewModel = new ToDo
                {
                    WorkTodo = todo.WorkTodo,
                    DescriptionTodo = todo.DescriptionTodo,
                    TitleTodo = todo.TitleTodo,
                    StartTime = todo.StartTime,
                    EndTime =todo.EndTime,
                    IsCompleted = todo.IsCompleted
                };
                //var mappedViewModel = _mapper.Map<Todo>(todo); 
                int result = await _todoService.Add(mappedViewModel).ConfigureAwait(false);
                return Ok(result);
            }

            return BadRequest();
        }
        [Route("deletetodo/{Id}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteTodo(int Id)
        {
            if (Id != 0)
            {
                return Ok(await _todoService.Delete(Id).ConfigureAwait(false));
            }
            return BadRequest();
        }

        [Route("updateetodo/{Id}")]
        [HttpPut]
        public async Task<IActionResult> UpdateTodo(int Id, [FromBody] TodoUpdateViewModel todo)
        {
            if (Id != 0 && todo != null)
            {
                // NOTE: Used AutoMapper for Object-to-Object mapping instead of old school viewModel 
                // var mappedViewModel = new Todo  
                // {  
                //     WorkTodo = todo.WorkTodo,
                //     IsCompleted = todo.IsCompleted
                // }; 

                var mappedViewModel = _mapper.Map<ToDo>(todo);

                return Ok(await _todoService.Update(Id, mappedViewModel).ConfigureAwait(false));
            }
            return BadRequest();
        }
        [Route("completedtodo/{Id}/{Completed}")]
        [HttpPut]
        public async Task<IActionResult> MarkCompleted(int Id, bool Completed)
        {
            if (Id != 0)
            {
                return Ok(await _todoService.MarkCompleted(Id, Completed).ConfigureAwait(false));
            }
            return BadRequest();
        }
    }
}