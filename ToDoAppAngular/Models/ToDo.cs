﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoAppAngular.Models
{
    public class ToDo
    {
        [Key]
        public int Id { get; set; }
        public string WorkTodo { get; set; }
        public string DescriptionTodo { get; set; }
        public string TitleTodo { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime?  StartTime{ get; set; }
        public DateTime?  EndTime{ get; set; }
        public User User { get; set; }
    }
}
