﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoAppAngular.Models
{
    public class User : IdentityUser
    {
        public Role Role { get; set; }
    }
}
