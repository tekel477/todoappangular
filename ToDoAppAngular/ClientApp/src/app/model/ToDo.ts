export class ToDo {
    id: Number | undefined;
    workTodo: string = '';
    DescriptionTodo: string = '';
    TitleTodo: string = '';
    StartTime: Date | undefined;
    EndTime: Date | undefined;
    isCompleted: boolean = false;

    // constructor(values: Object = {}) {
    //     Object.assign(this, values);
    // }

    // constructor(workTodo: string, isCompleted: boolean, id?: number){
    //   this.id = id;
    //   this.workTodo = workTodo;
    //   this.isCompleted = isCompleted;
    // }

}
