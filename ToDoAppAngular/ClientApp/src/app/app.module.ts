//import { NgModule } from '@angular/core';
//import { BrowserModule } from '@angular/platform-browser';

//import { AppRoutingModule } from './app-routing.module';
//import { AppComponent } from './app.component';
//import { TodoListComponent } from './todo-list/todo-list.component';
//import { ApiComponent } from './api/api.component';
//import { ComponentsComponent } from './components/components.component';
//import { ModelComponent } from './model/model.component';
//import { ServicesComponent } from './services/services.component';
//import { NavMenuComponent } from './components/nav-menu/nav-menu.component';

//@NgModule({
//  declarations: [
//    AppComponent,
//    TodoListComponent,
//    ApiComponent,
//    ComponentsComponent,
//    ModelComponent,
//    ServicesComponent,
//    NavMenuComponent
//  ],
//  imports: [
//    BrowserModule,
//    AppRoutingModule
//  ],
//  providers: [],
//  bootstrap: [AppComponent]
//})
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { ToDoListComponent } from './components/todo-list/todo-list.component';

@NgModule({

    declarations: [
        AppComponent,
        NavMenuComponent,
        ToDoListComponent,
        
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', component: ToDoListComponent, pathMatch: 'full' },
            { path: 'login', component: ToDoListComponent, pathMatch: 'full' },
        ])
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
