import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError } from 'rxjs';

import { ToDo } from '../model/ToDo';
import { getLocaleDateFormat } from '@angular/common';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
}

@Injectable({
    providedIn: 'root'
})
export class ToDoService {

    lastId: number = 0;
    workTodo: string = '';
    todos: ToDo[] = [];

    private readonly todosEndpoint: string = "/api/todo/";

    constructor(private http: HttpClient) {
        this.todos = this.getTodos();
    }

    private errorHandler(error: HttpErrorResponse) {
        return observableThrowError(error.message || 'Something went wrong!');
    }

    // Get Todo List
    public getTodos(): ToDo[] {

        this.http.get<ToDo[]>(this.todosEndpoint + 'getTodoAll')
            .pipe(catchError(this.errorHandler))
            .subscribe((response: any) => {
                this.todos = response;
            });
        return this.todos;
    }

    // Add A Todo
    public addTodo(workTodo: string): void {

        if (workTodo.trim() === "" && workTodo.trim().length === 0) {
            return;
        }

        const payload = {
            workTodo: workTodo,
            isCompleted: false
        };

        this.http.post(this.todosEndpoint + "addtodo", payload, httpOptions).subscribe((response: any) => {

            return this.todos.push({
                id: response,
                workTodo: workTodo,
                isCompleted: false,
                StartTime: new Date(),
                EndTime: new Date(),
                DescriptionTodo: "",
                TitleTodo: ""
            });

        });
    }

    // Update A Todo
    public updateTodo(id: number, todo: any): void {

        if (id <= 0 && todo.workTodo.trim() === "" && todo.workTodo.trim().length === 0) {
            return;
        }

        const payload = {
            workTodo: todo.workTodo,
            isCompleted: todo.isCompleted
        };

        this.http.put(this.todosEndpoint + "updateetodo/" + id, payload, httpOptions).subscribe((response: any) => {

            console.log(response);

            const index = this.todos.findIndex((e) => e.id === id);

            this.todos[index].workTodo = payload.workTodo;

        });
    }

    // Delete A Todo
    deleteTodo({ todo, index }: { todo: ToDo; index: number; }): Promise<Object> {
        if (todo.id != 0) {
            this.todos.splice(index, 1);
        }

            return new Promise((resolve) => {
                this.http.delete(this.todosEndpoint + "deletetodo/" + todo.id, httpOptions).subscribe((response: any) => {
                    resolve(response);
                });
            });

        }

    

    // Mark Todo As Completed
    markAsCompleted(todo: ToDo, evnt: string): Promise<Object> {

        console.log(todo.id + " " + evnt);

        const payload = {
            IsCompleted: evnt
        };

        return new Promise((resolve) => {
            this.http.put(this.todosEndpoint + "completedtodo/" + todo.id + "/" + evnt, payload, httpOptions).subscribe((response: any) => {
                resolve(response);
            });
        });

    }

}
