﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoAppAngular.Models;
using ToDoAppAngular.Repository.Interface;

namespace ToDoAppAngular.Service
{
    public class TodoService : IToDoService
    {
        private readonly ITodoRepository _repository;

        public TodoService() { }
        public TodoService(ITodoRepository repository)
        {
            this._repository = repository;
        }

        public Task<int> Add(ToDo todo)
        {
            return _repository.Add(todo);
        }

        public Task<bool> Delete(int Id)
        {
            return _repository.Delete(Id);
        }

        public Task<List<ToDo>> GetAll()
        {
            return _repository.GetAll();
        }

        public Task<bool> MarkCompleted(int Id, bool IsCompleted)
        {
            return _repository.MarkCompleted(Id, IsCompleted);
        }

        public Task<bool> Update(int Id, ToDo todo)
        {
            return _repository.Update(Id, todo);
        }
    }


}