﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoAppAngular.Models;
namespace ToDoAppAngular.Repository.Interface
{
    public interface ITodoRepository
    {
        Task<List<ToDo>> GetAll();
        Task<int> Add(ToDo todo);
        Task<bool> Update(int Id, ToDo todo);
        Task<bool> Delete(int Id);
        Task<bool> MarkCompleted(int Id, bool IsCompleted);
    }
}
