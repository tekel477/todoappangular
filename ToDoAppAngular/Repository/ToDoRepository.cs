﻿using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToDoAppAngular.Models;
using ToDoAppAngular.Data;
using ToDoAppAngular.Repository.Interface;

public class TodoRepository : ITodoRepository, IDisposable
{
    private readonly ApplicationDbContext _context;

    public TodoRepository(ApplicationDbContext context)
    {
        this._context = context;
    }

    public async Task<List<ToDo>> GetAll()
    {
        return await _context.ToDo.ToListAsync();
    }

    public async Task<int> Add(ToDo todo)
    {
        await _context.ToDo.AddAsync(todo);
        await _context.SaveChangesAsync();
        return todo.Id;
    }

    public Task<ToDo> Find(int Id)
    {
        return _context.ToDo.FindAsync(Id).AsTask();
    }

    public async Task<bool> Update(int Id, ToDo newTodo)
    {
        var todo = await Find(Id);
        todo.WorkTodo = newTodo.WorkTodo;
        todo.IsCompleted = newTodo.IsCompleted;
        await _context.SaveChangesAsync();
        return true;
    }

    public async Task<bool> Delete(int Id)
    {
        var todo = await Find(Id);
        _context.Remove(todo);
        await _context.SaveChangesAsync();
        return true;
    }


    public async Task<bool> MarkCompleted(int Id, bool IsCompleted)
    {
        var todo = await Find(Id);
        todo.IsCompleted = IsCompleted;
        await _context.SaveChangesAsync();
        return true;
    }

    #region Dispose method
    private bool disposed = false;
    protected virtual void Dispose(bool disposing)
    {
        if (!this.disposed)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
        this.disposed = true;
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    #endregion


}